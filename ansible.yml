- hosts: all
  become: true
  tasks:
    - name: Install apt-transport-https
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - lsb-release
          - gnupg
        state: latest
        update_cache: true

    - name: Add signing key
      ansible.builtin.apt_key:
        url: "https://download.docker.com/linux/{{ ansible_distribution | lower }}/gpg"
        state: present

    - name: Add repository into sources list
      ansible.builtin.apt_repository:
        repo: "deb [arch={{ ansible_architecture }}] https://download.docker.com/linux/{{ ansible_distribution | lower }} {{ ansible_distribution_release }} stable"
        state: present
        filename: docker

    - name: Install Docker
      ansible.builtin.apt:
        name:
          - docker
          - docker.io
          - docker-compose
          - docker-registry
        state: latest
        update_cache: true

    - name: Create deployer user
      user:
        name: deployer
        comment: Deployer user
        group: admin
        groups: sudo, docker

    - name: Add public key to authorized_keys
      authorized_key:
        user: "deployer"
        state: present
        key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"

    - name: ensure nginx is at the latest version
      apt: name=nginx state=latest
    
    - name: start nginx
      service:
          name: nginx
          state: started

    - name: copy nginx config file
      copy: src=nginx-conf-proxy.com dest=/etc/nginx/sites-available/nginx-conf-proxy.com

    - name: enable configuration
      file: >
        dest=/etc/nginx/sites-enabled/nginx-conf-proxy.com
        src=/etc/nginx/sites-available/nginx-conf-proxy.com
        state=link
    
    - name: restart nginx
      service:
          name: nginx
          state: restarted

    - name: Install Gitlab Runner
      ansible.builtin.apt:
        name:
          - gitlab-runner
        state: latest
        update_cache: true

    - name: List configured runners
      command: gitlab-runner list
      register: configured_runners
      changed_when: False

    - name: Register runner to deploy with GitLab
      command: gitlab-runner register >
        --non-interactive
        --url https://gitlab.com/
        --registration-token GR13489411jxTqaeWz29gFPRbw-1R
        --description "DeploymentRunner"
        --tag-list deploymentDocker
        --executor docker
        --docker-image "docker:stable"
      when: configured_runners.stderr.find('\nDeploymentRunner') == -1
    
    - name: Register runner for CI
      command: gitlab-runner register >
        --non-interactive
        --url https://gitlab.com/
        --registration-token GR13489411jxTqaeWz29gFPRbw-1R
        --description "BuildTestRunnerNet6"
        --tag-list buildTestDocker
        --executor docker
        --docker-image "mcr.microsoft.com/dotnet/sdk:6.0"
      when: configured_runners.stderr.find('\nBuildTestRunnerNet6') == -1