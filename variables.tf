variable "do_token" {
  type    = string
}

variable "pub_key_path" {
  type    = string
}

variable "pvt_key_path" {
  type    = string
}
