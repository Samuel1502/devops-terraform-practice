resource "digitalocean_ssh_key" "deployer"{
  name       = "Deployer"
  public_key = file(var.pub_key_path)
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "devops-iaac"
  region = "sfo3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    digitalocean_ssh_key.deployer.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key_path)
    timeout = "2m"
  }
  
  provisioner "remote-exec" {
    inline = ["echo wait for ssh connection"]
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i root@${self.ipv4_address}, --private-key ${var.pvt_key_path} ansible.yml"
  }
  # ansible-playbook -i root@147.182.255.205, --private-key /home/samuel/.ssh/id_rsa ansible.yml
}